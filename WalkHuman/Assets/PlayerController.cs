﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //移動速度
    public float walkSpeed = 1000.0f;


    //キャラクターの移動を扱うコンポーネント
    CharacterController charCtrl;

    //アニメーションの切り替えを扱うコンポーネント
    Animator anim;



    //最初
    void Start()
    {
        //コンポーネントを取得
        charCtrl = GetComponent<CharacterController>();
        anim = transform.GetChild(0).GetComponent<Animator>();
    }




    //常に
    void Update()
    {
        //入力状況から移動ベクトルを作成
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //移動速度の調整
        move *= (Time.deltaTime * walkSpeed);

        //進行方向を向ける
        transform.LookAt(transform.position + move);

        //移動
        charCtrl.SimpleMove(move);

        //アニメーション切り替えコンポーネントに現在の速度を渡す
        anim.SetFloat("speed", move.magnitude);
    }
}
