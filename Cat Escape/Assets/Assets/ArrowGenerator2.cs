﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowGenerator2:MonoBehaviour
{
    public GameObject arrowPrefab2;
    float span = 0.7f;
    float delta = 0;

    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(arrowPrefab2) as GameObject;
            int px = Random.Range(-6, 7);
            go.transform.position = new Vector3(px, -5, 0);

        }
    }
	
}