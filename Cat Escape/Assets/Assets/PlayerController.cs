﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    void Start()
    {

    }

    void Update()
    {
        //左矢印が押されたとき
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-0.07f, 0, 0);//左に1動く
        }
        //右
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(0.07f, 0, 0);//右に1動く
        }
        //上
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, 0.07f, 0);
        }
        //下
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0, -0.07f, 0);
        }
    }
}
