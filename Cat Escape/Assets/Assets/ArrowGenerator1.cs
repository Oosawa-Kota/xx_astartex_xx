﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowGenerator1:MonoBehaviour
{
    public GameObject arrowPrefab;
    float span = 0.7f;
    float delta = 0;

    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(arrowPrefab) as GameObject;
            int px = Random.Range(-4, 4);
            int py = Random.Range(-3, 3);
            go.transform.position = new Vector3(px, py, 0);

        }
    }
	
}