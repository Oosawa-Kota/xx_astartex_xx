#pragma once
class Enemy
{
	int hp, atk, def;
public:
	Enemy();
	void DispHp();
	int Attack(int i);
	void Damage(int i);
	int GetDef();
	bool IsDead();
};
