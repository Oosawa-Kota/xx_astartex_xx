#include<stdio.h>
#include<conio.h>
#include"Plyer.h"
#include"Enemy.h"

void main()
{
	Plyer pl;
	Enemy ene;

	int damage;
	for (int turn = 1;; turn++)
	{
		printf("\n======%dターン目======\n", turn);
		pl.DispHp();
		ene.DispHp();
		//
		damage = pl.Attack(ene.GetDef());
		ene.Damage(damage);
		if (ene.IsDead() == true) break;
		damage = ene.Attack(pl.GetDef());
		pl.Damage(damage);
		if (pl.IsDead() == true) break;
	}
	printf("終了\n");
	_getch();
}