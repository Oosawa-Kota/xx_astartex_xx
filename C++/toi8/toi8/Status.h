#pragma once
class Status
{
	int lv, hp, atk, def;

public:
	int SetLv(int i);
	void Calc();
	int GetHp();
	int GetAtk();
	int GetDef();
};
