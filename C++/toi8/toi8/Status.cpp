#include"Status.h"

int Status::SetLv(int i)
{
	lv = i;
	if (lv<100)
	{
		return lv;
	}
	else
	{
		lv = 99;
		return lv;
	}
}
void Status::Calc()
{
	hp = lv * lv + 50;
	atk = lv * 10;
	def = lv * 9;
}
int Status::GetHp()
{
	return hp;
}
int Status::GetAtk()
{
	return atk;
}
int Status::GetDef()
{
	return def;
}