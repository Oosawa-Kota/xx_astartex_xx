﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiplicationTable
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("   * * * 九九の表 * * *  ");
            Console.WriteLine("   |  1  2  3  4  5  6  7  8  9 ");
            Console.WriteLine("--------------------------------");
            
            
            for (int a = 1; a < 10; a++)
            {

                Console.Write(a + "  |");
             
                    for (int b = 1; b < 10; b++)
                    {
                        //Console.Write(a + "  |");
                        int c = a * b;
                        Console.Write($"{c,3}");
                        

                    }
                Console.WriteLine("  ");

            }
            Console.ReadLine();
            

        }
    }
}
