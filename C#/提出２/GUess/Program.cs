﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUess
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 32, b = 0, c = 100,d=0,e=1;
            while(d!=a)//答えと入力値が不一致の場合
            {
                Console.Write(b + "から" + c + "の間の数値を当ててください。＞");
                d = int.Parse(Console.ReadLine());//入力値を代入

                if (d < a)//入力値より答えが大きい
                {
                    Console.WriteLine("答はもっと大きいです。");
                    b = d;//入力値を最小条件値に代入
                }

                else if (d == a)
                    break;//答えと一致

                else//入力値より答えが小さい
                {
                    Console.WriteLine("答はもっと小さいです。");
                    c = d;//入力値を最大条件値に代入
                }
                e++;
                Console.WriteLine();
            }
            Console.WriteLine("おめでとう。" + e + "回目で当てました。");
            Console.WriteLine();
            Console.WriteLine("終了するには何かキーを押してください。");
           Console.Read();

        }
    }
}
