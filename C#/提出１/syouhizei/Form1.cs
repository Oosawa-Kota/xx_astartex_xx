﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace syouhizei
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void buttonAddTax_Click_Click(object sender, EventArgs e)
        {
            int money;//入力出力の変数
            double addTax;//計算用の変数
            const double Tax=0.1;//消費税

            money = int.Parse(textBox1.Text);//テキストボックスの数字を読み込む

            addTax = money;//ダブル型に数字を代入
            addTax *= (1 + Tax);//アドタックスに1.1倍をかける
            money = (int)addTax;//小数点以下を切り捨てて代入

            label4.Text = money + "円";
        }
    }
}
