﻿namespace BATTLE
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.Player_1 = new System.Windows.Forms.RadioButton();
            this.Player_2 = new System.Windows.Forms.RadioButton();
            this.Player_3 = new System.Windows.Forms.RadioButton();
            this.Enemy_1 = new System.Windows.Forms.RadioButton();
            this.Enemy_2 = new System.Windows.Forms.RadioButton();
            this.Enemy_3 = new System.Windows.Forms.RadioButton();
            this.PlayerGroup1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.P_LV = new System.Windows.Forms.NumericUpDown();
            this.PlayerLevel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.PlayerGroup1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.P_LV)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // Player_1
            // 
            this.Player_1.AutoSize = true;
            this.Player_1.Location = new System.Drawing.Point(6, 18);
            this.Player_1.Name = "Player_1";
            this.Player_1.Size = new System.Drawing.Size(88, 16);
            this.Player_1.TabIndex = 0;
            this.Player_1.TabStop = true;
            this.Player_1.Text = "radioButton1";
            this.Player_1.UseVisualStyleBackColor = true;
            this.Player_1.CheckedChanged += new System.EventHandler(this.Player_1_CheckedChanged);
            // 
            // Player_2
            // 
            this.Player_2.AutoSize = true;
            this.Player_2.Location = new System.Drawing.Point(6, 40);
            this.Player_2.Name = "Player_2";
            this.Player_2.Size = new System.Drawing.Size(88, 16);
            this.Player_2.TabIndex = 1;
            this.Player_2.TabStop = true;
            this.Player_2.Text = "radioButton2";
            this.Player_2.UseVisualStyleBackColor = true;
            // 
            // Player_3
            // 
            this.Player_3.AutoSize = true;
            this.Player_3.Location = new System.Drawing.Point(6, 62);
            this.Player_3.Name = "Player_3";
            this.Player_3.Size = new System.Drawing.Size(88, 16);
            this.Player_3.TabIndex = 2;
            this.Player_3.TabStop = true;
            this.Player_3.Text = "radioButton3";
            this.Player_3.UseVisualStyleBackColor = true;
            // 
            // Enemy_1
            // 
            this.Enemy_1.AutoSize = true;
            this.Enemy_1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Enemy_1.Location = new System.Drawing.Point(6, 18);
            this.Enemy_1.Name = "Enemy_1";
            this.Enemy_1.Size = new System.Drawing.Size(88, 16);
            this.Enemy_1.TabIndex = 3;
            this.Enemy_1.TabStop = true;
            this.Enemy_1.Text = "radioButton4";
            this.Enemy_1.UseVisualStyleBackColor = true;
            this.Enemy_1.CheckedChanged += new System.EventHandler(this.Enemy_1_CheckedChanged);
            // 
            // Enemy_2
            // 
            this.Enemy_2.AutoSize = true;
            this.Enemy_2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Enemy_2.Location = new System.Drawing.Point(6, 40);
            this.Enemy_2.Name = "Enemy_2";
            this.Enemy_2.Size = new System.Drawing.Size(88, 16);
            this.Enemy_2.TabIndex = 4;
            this.Enemy_2.TabStop = true;
            this.Enemy_2.Text = "radioButton5";
            this.Enemy_2.UseVisualStyleBackColor = true;
            this.Enemy_2.CheckedChanged += new System.EventHandler(this.Enemy_2_CheckedChanged);
            // 
            // Enemy_3
            // 
            this.Enemy_3.AutoSize = true;
            this.Enemy_3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Enemy_3.Location = new System.Drawing.Point(6, 62);
            this.Enemy_3.Name = "Enemy_3";
            this.Enemy_3.Size = new System.Drawing.Size(88, 16);
            this.Enemy_3.TabIndex = 5;
            this.Enemy_3.TabStop = true;
            this.Enemy_3.Text = "radioButton6";
            this.Enemy_3.UseVisualStyleBackColor = true;
            this.Enemy_3.CheckedChanged += new System.EventHandler(this.Enemy_3_CheckedChanged);
            // 
            // PlayerGroup1
            // 
            this.PlayerGroup1.Controls.Add(this.label5);
            this.PlayerGroup1.Controls.Add(this.label4);
            this.PlayerGroup1.Controls.Add(this.label3);
            this.PlayerGroup1.Controls.Add(this.P_LV);
            this.PlayerGroup1.Controls.Add(this.PlayerLevel);
            this.PlayerGroup1.Controls.Add(this.Player_1);
            this.PlayerGroup1.Controls.Add(this.Player_2);
            this.PlayerGroup1.Controls.Add(this.Player_3);
            this.PlayerGroup1.Location = new System.Drawing.Point(65, 47);
            this.PlayerGroup1.Name = "PlayerGroup1";
            this.PlayerGroup1.Size = new System.Drawing.Size(106, 261);
            this.PlayerGroup1.TabIndex = 6;
            this.PlayerGroup1.TabStop = false;
            this.PlayerGroup1.Text = "プレイヤー";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(6, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "label5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(6, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(6, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "label3";
            // 
            // P_LV
            // 
            this.P_LV.Location = new System.Drawing.Point(6, 111);
            this.P_LV.Name = "P_LV";
            this.P_LV.Size = new System.Drawing.Size(88, 19);
            this.P_LV.TabIndex = 4;
            this.P_LV.ValueChanged += new System.EventHandler(this.P_LV_ValueChanged);
            // 
            // PlayerLevel
            // 
            this.PlayerLevel.AutoSize = true;
            this.PlayerLevel.Location = new System.Drawing.Point(7, 95);
            this.PlayerLevel.Name = "PlayerLevel";
            this.PlayerLevel.Size = new System.Drawing.Size(34, 12);
            this.PlayerLevel.TabIndex = 3;
            this.PlayerLevel.Text = "レベル";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.numericUpDown2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.Enemy_1);
            this.groupBox2.Controls.Add(this.Enemy_2);
            this.groupBox2.Controls.Add(this.Enemy_3);
            this.groupBox2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.Location = new System.Drawing.Point(200, 47);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(106, 261);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "敵";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(7, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "label6";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.numericUpDown2.Location = new System.Drawing.Point(6, 111);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(88, 19);
            this.numericUpDown2.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(7, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 9;
            this.label7.Text = "label7";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(7, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "レベル";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(7, 165);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 16);
            this.label8.TabIndex = 8;
            this.label8.Text = "label8";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(151, 326);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(65, 365);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(241, 147);
            this.textBox1.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 545);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.PlayerGroup1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.PlayerGroup1.ResumeLayout(false);
            this.PlayerGroup1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.P_LV)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton Player_1;
        private System.Windows.Forms.RadioButton Player_2;
        private System.Windows.Forms.RadioButton Player_3;
        private System.Windows.Forms.RadioButton Enemy_1;
        private System.Windows.Forms.RadioButton Enemy_2;
        private System.Windows.Forms.RadioButton Enemy_3;
        private System.Windows.Forms.GroupBox PlayerGroup1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown P_LV;
        private System.Windows.Forms.Label PlayerLevel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
    }
}

