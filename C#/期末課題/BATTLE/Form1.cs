﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BATTLE
{
   
    public partial class Form1 : Form
    {
        int p_PH, p_ATK, p_DEF, e_PH, e_ATK, e_DEF;

        int lv;

        

        string s;




        //private string p_ATK, p_DEF, e_PH, e_ATK, e_DEF;

        private void Enemy_1_CheckedChanged(object sender, EventArgs e)
        {
            s = "1";
            E_Pattern(s,lv);
            label8.Text = e_PH + " \n";
            label7.Text = e_ATK + "\n ";
            label6.Text = e_DEF + " \n";
        }

        private void Enemy_2_CheckedChanged(object sender, EventArgs e)
        {
            s = "2";
            E_Pattern(s,lv);
            label8.Text = e_PH + " \n";
            label7.Text = e_ATK + " \n";
            label6.Text = e_DEF + "\n ";
        }

        private void Enemy_3_CheckedChanged(object sender, EventArgs e)
        {
            s = "3";
            E_Pattern(s,lv);
            label8.Text = e_PH + " \n";
            label7.Text = e_ATK + " \n";
            label6.Text = e_DEF + " \n";
        }

        private void Player_3_CheckedChanged(object sender, EventArgs e)
        {
            s = "3";
            P_Pattern(s,lv);
            label3.Text = p_PH + " \n";
            label4.Text = p_ATK + " \n";
            label5.Text = p_DEF + " \n";
        }

        private void Player_2_CheckedChanged(object sender, EventArgs e)
        {
            s = "2";
            P_Pattern(s,lv);
            label3.Text = p_PH + " \n";
            label4.Text = p_ATK + " \n";
            label5.Text = p_DEF + " \n";

        }


        private void Player_1_CheckedChanged(object sender, EventArgs e)
        {
            s = "1";
            P_Pattern(s,lv);
        }

        private void P_LV_ValueChanged(object sender, EventArgs e)
        {
            
            P_Pattern(s,lv);
        }

        public Form1()
        {
         
            InitializeComponent();
        }

        public void P_Pattern(string i,int lv)
        {

            switch (i)
            {
                case "1":
                    p_PH = 12 * lv;
                    p_ATK = 5 * lv;
                    p_DEF = 4 * lv;
                    break;
                case "2":
                    p_PH = 13 * lv;
                    p_ATK = 4 * lv;
                    p_DEF = 5 * lv;
                    break;
                case "3":
                    p_PH = 10 * lv;
                    p_ATK = 3 * lv;
                    p_DEF = 8 * lv;
                    break;
                default:
                    p_PH = 0 * lv;
                    p_ATK = 0 * lv;
                    p_DEF = 0 * lv;
                    break;
            }
            label3.Text = p_PH + " \n";
            label4.Text = p_ATK + " \n";
            label5.Text = p_DEF + " \n";
        }
        
        public void E_Pattern(string i,int lv)
        {
            switch (i)
            {
                case "1":
                    e_PH = 12 * lv;
                    e_ATK = 5 * lv;
                    e_DEF = 4 * lv;
                    break;
                case "2":
                    e_PH = 13 * lv;
                    e_ATK = 4 * lv;
                    e_DEF = 5 * lv;
                    break;
                case "3":
                    e_PH = 10 * lv;
                    e_ATK = 3 * lv;
                    e_DEF = 8 * lv;
                    break;
                default:
                    e_PH = 0 * lv;
                    e_ATK = 0 * lv;
                    e_DEF = 0 * lv;
                    break;
            }
            label8.Text = e_PH + " \n";
            label7.Text = e_ATK + " \n";
            label6.Text = e_DEF + " \n";
        }
        void Leval()
        {
            int v = Toint(P_LV.Value);
            lv = v;
        }

        private int Toint(decimal value)
        {
            throw new NotImplementedException();
        }
    }
    
}
