﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceSample5
{
    // 基本クラス
    class Class1
    {
        public virtual void M()
        {
            Console.WriteLine("基本クラスのMメソッドが呼ばれました。");
        }
    }
}
