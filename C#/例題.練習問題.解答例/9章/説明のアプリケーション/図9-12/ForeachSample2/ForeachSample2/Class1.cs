﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForeachSample2
{
    class Class1
    {
        public Class1(int number)
        {
            Number = number;
        }

        public int Number { get; set; }
    }
}
