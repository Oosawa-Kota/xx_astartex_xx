﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    // スコアを表示する
    public Text scoreText;
    // スコア
    private int score;

    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        // スコアを表示する
        scoreText.text = score.ToString();
    }

    private void Initialize()
    {
        // スコアを0に戻す
        score = 0;
    }

    // ポイントの追加
    public void AddPoint(int point)
    {
        score = score + point;
    }
    public void HalfPoint()
    {
        score = score /2 ;
    }

}
