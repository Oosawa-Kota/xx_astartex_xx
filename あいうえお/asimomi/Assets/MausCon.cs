﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MausCon : MonoBehaviour
{

    enum TOUCH_TYPE
    {
        NO_TOUCH,   //どこも触ってない
        HIT,        //ツボ触ってる
        HIP,        //お尻触ってる
        OUT         //足のツボじゃないとこ触ってる
    };


    TOUCH_TYPE flag = TOUCH_TYPE.NO_TOUCH;

    public GameObject[] prefabs;
    GameObject touchColor;
    private Vector3 position;
    private Vector3 screenToWorldPointPosition;
    GameObject lastTouchLocation;
    public AudioClip[] sound;
    AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
 
        position = Input.mousePosition;
        position.z = 10.0f;
        gameObject.transform.position = Camera.main.ScreenToWorldPoint(position);



        if (Input.GetMouseButtonDown(0))
        {

            if (flag == TOUCH_TYPE.HIT)
            {
                
                
                    if (touchColor) Destroy(touchColor.gameObject);
                    touchColor = Instantiate(prefabs[0], transform.position, transform.rotation);
                
                FindObjectOfType<Score>().AddPoint(10);
                Destroy(lastTouchLocation);
                audioSource.PlayOneShot(sound[Random.Range(0, 2)]);
                flag = 0;
            }
            else if (flag == TOUCH_TYPE.NO_TOUCH)
            {
                audioSource.PlayOneShot(sound[2]);
            }


            //お尻触った
            else
            {
                FindObjectOfType<Score>().HalfPoint();//ポイント半減
                audioSource.PlayOneShot(sound[Random.Range(3, 7)]);

                FindObjectOfType<Background>().Shake();
            }
        }
        //if (Input.GetMouseButtonUp(0))
        //{
        //    flag = 0;
        //}
        
            

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Hip")
        {
            flag = TOUCH_TYPE.HIP;
        }



        else
        {
            flag = TOUCH_TYPE.HIT;
            lastTouchLocation = collision.gameObject;
        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        flag = 0;
        //Debug.Log("fgerhg");
    }
}
