﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TanCon : MonoBehaviour
{
    float belta = 2.0f;
    float lifeTame = 0.0f;
   


    // Start is called before the first frame update
    void Start()
    {
  
    }

    // Update is called once per frame
    void Update()
    {

            this.lifeTame += Time.deltaTime;
            if (this.belta < this.lifeTame)
            {
                lifeTame = 0;
                Destroy(gameObject);
            }
    }
    void OnCollisionEnter(Collision collision)
    {
        //衝突判定
        if (collision.gameObject.tag == "YUBI")
        {
            //スコア処理を追加
            FindObjectOfType<Score>().AddPoint(10);

            //相手のタグがnameならば、自分を消す
            Destroy(this.gameObject);
        }
    }

}
    
