﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    Vector3 basicPosition;

    //震えてるかフラグ
    bool shakeFlag = false;

    float shakeTimer = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        //最初の位置を覚えておく
        basicPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //震えてる
        if (shakeFlag == true)
        {
            float x = Random.Range(-0.5f, 0.5f);
            float y = Random.Range(-0.5f, 0.5f);
            transform.position = basicPosition + new Vector3(x, y, 0);  //最初の位置からランダムでずらす

            //タイマーをカウントダウン
            shakeTimer -= Time.deltaTime;

            //タイマーが0を切ったら
            if(shakeTimer < 0)
            {
                //通常
                shakeFlag = false;
            }
        }

        //通常
        else
        {
            transform.position = basicPosition;
        }
    }

    //このメソッドが呼ばれれば震えスタート
    public void Shake()
    {
        shakeFlag = true;
        shakeTimer = 0.5f;
    }
}
