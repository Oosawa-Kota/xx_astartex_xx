﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFX_Test : MonoBehaviour
{
    public GameObject[] prefabs;
    GameObject effect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (prefabs.Length > 0)
            {
                if(effect)  Destroy(effect.gameObject);
                effect = Instantiate(prefabs[0], transform.position, transform.rotation);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (prefabs.Length > 1)
            {
                if (effect) Destroy(effect.gameObject);
                effect = Instantiate(prefabs[1], transform.position, transform.rotation);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (prefabs.Length > 2)
            {
                if (effect) Destroy(effect.gameObject);
                effect = Instantiate(prefabs[2], transform.position, transform.rotation);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (prefabs.Length > 3)
            {
                if (effect) Destroy(effect.gameObject);
                effect = Instantiate(prefabs[3], transform.position, transform.rotation);
            }
        }


    }
}
