﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarContorar : MonoBehaviour
{
    public GameObject BombPrefab;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            this.transform.Translate(0, 0.7f, 0);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            transform.Translate(0f, -0.7f, 0f);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.Translate(0.7f, 0f, 0f);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.Translate(-0.7f, 0f, 0f);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject go = Instantiate(BombPrefab) as GameObject;
            go.transform.position = transform.position;
        }
    } 
}
